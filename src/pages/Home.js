// libraries
import { useContext } from 'react';
import { Link, Navigate } from 'react-router-dom';

// styles
import 'bootstrap/dist/css/bootstrap.min.css';
import { Carousel, Button } from 'react-bootstrap';
import '../styles/Home.css';

// context
import UserContext from '../UserContext';

// images
import logo1 from '../img/motofit-banner.svg';
import logo2 from '../img/motofit-banner-ams.svg';
import ris from '../img/ris.png';
import SMK from '../img/brands/SMK.svg';
import AGV from '../img/brands/AGV.svg';
import HJC from '../img/brands/HJC.svg';
import EVO from '../img/brands/EVO.svg';
import LS2 from '../img/brands/LS2.svg';

export default function Home() {

	// getting global state/value of id and isAdmin
	const { idGlobal, isAdminGlobal} = useContext(UserContext);

	return (
		// conditional rendering
		(isAdminGlobal === false || idGlobal === null) ?
		<>
			<header className="container-fluid">
				<section className="row">
					<div className="col bg-light">
						<img className="img-fluid" src={logo1} alt="MOTOFIT"/>
					</div>
				</section>
			</header>
			<main className="container-fluid">
				<section className="row">
					<div className="col p-5 bg-dark" id="ris-banner">
						<img className="img-fluid rounded" src={ris} alt="MOTOFIT"/>
						<Button className="w-100 rounded-pill mt-3" as={ Link } to="/products">SHOP NOW!</Button>
					</div>
					<div className="col-lg-4 p-5 bg-light text-dark">
						<h2>Top Helmet Brands</h2>
						<Carousel className="rounded bg-secondary">	
							<Carousel.Item>
								<img className="img-fluid" src={SMK} alt="MOTOFIT"/>
							</Carousel.Item>
							<Carousel.Item>
								<img className="img-fluid" src={AGV} alt="MOTOFIT"/>
							</Carousel.Item>
							<Carousel.Item>
								<img className="img-fluid" src={HJC} alt="MOTOFIT"/>
							</Carousel.Item>
							<Carousel.Item>
								<img className="img-fluid" src={EVO} alt="MOTOFIT"/>
							</Carousel.Item>
							<Carousel.Item>
								<img className="img-fluid" src={LS2} alt="MOTOFIT"/>
							</Carousel.Item>
						</Carousel>
					</div>
				</section>
			</main>		
		</>
		: <>
			<header className="container-fluid">
				<section className="row">
					<div className="col bg-light">
						<img className="img-fluid" src={logo2} alt="MOTOFIT"/>
					</div>
				</section>
			</header>
		</>
		// end of conditional rendering
		
	);
};