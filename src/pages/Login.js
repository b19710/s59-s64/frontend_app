// libraries
import { useState, useEffect, useContext } from 'react';
import { Link, Navigate } from 'react-router-dom';
import { Form, Button, Nav } from 'react-bootstrap';

// styles
import '../styles/Login.css';

// context
import UserContext from '../UserContext';

// misc libraries
import Swal from 'sweetalert2';

export default function Login() {

	// getting global state/value of id and isAdmin
	const { idGlobal, /*isAdminGlobal,*/ setIdGlobal, setIsAdminGlobal } = useContext(UserContext);

	
	// init states/values
	const [email, setEmail] = useState(() =>'');
	const [password, setPassword] = useState(() => '');
	const [displayPassword, setDisplayPassword] = useState(() => false);
	const [disableBtn, setDisableBtn] = useState(() => true)


	// side effect for login button
	useEffect(() => {
		(email !== '' && password !== '')
		? setDisableBtn(prevValue => false)
		: setDisableBtn(prevValue => true)
	},[email, password]);


	// fn to toggle on or off unmasking password
	const showPassword = () => {

		setDisplayPassword(prevVale => !displayPassword);
		return
	};


	const handleLogin = (e) => {

		e.preventDefault();

		// fetch(`${process.env.REACT_APP_API_URL}users/login`, {
		fetch(`${process.env.REACT_APP_API_URL}users/login`, {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password

			})
		})
		.then(res => res.json())
		.then(data => {

			// conditional state
			if (data.token === null) {
				
				Swal.fire({
					title: 'Login Failed',
					icon: 'error',
					text: 'No account found. You may try signing up first.'
				});
				return;
			} else if (data.token === '') {
				
				Swal.fire({
					title: 'Login Failed',
					icon: 'error',
					text: 'Either email and/or password is incorrect. Please try again.'
				});
				return;
			} else {

				// setting token
				localStorage.setItem('token', data.token);
				readDetails(localStorage.getItem('token'));

				// to clear email and password fields
				setEmail(prevValue => '');
				setPassword(prevValue =>'');
				
				Swal.fire({
						title: 'Welcome Back!',
						icon: 'success',
				});
				return;
			}
		});
	};

	const readDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}users/details`, {

		  headers: {
		    Authorization: `Bearer ${localStorage.getItem('token')}`
		  }
		})
		.then(res => res.json())
		.then(data => {

		  setIdGlobal(prevValue => data._id);
		  setIsAdminGlobal(prevVale => data.isAdmin);
		  return;
		})
		.catch(error => console.log(error));
		return;
	};

	return (
		(idGlobal === null)
		? <section className="container mt-5">
			<Form className="row justify-content-center" onSubmit={e => handleLogin(e)}>
				<div className="col-lg-6 p-5 border rounded shadow bg-light" id="form">
					<h1 className="mb-3 text-center">Login</h1>
					<Form.Group>
						<Form.Control
							className="mb-3"
							type="email"
							value={email}
							onChange={e => setEmail(e.target.value)}
							placeholder="Enter Email" />
						<Form.Control
							className="mb-2"
							type={(displayPassword) ? "text" : "password"}
							value={password}
							onChange={e => setPassword(e.target.value)} 
							placeholder="Enter Password" />
					</Form.Group>
					<Form.Group className="mb-5">
						<input className="form-check-input" type="checkbox" onClick={showPassword} id="showpw"/>
						<label htmlFor="showpw" className="mx-2">Show password</label>
					</Form.Group>
					{
						(disableBtn)
						? <Button variant="outline-danger" className="w-100 mb-5 rounded-pill" disabled>Sign In</Button>
						: <Button variant="outline-primary" type="submit" className="w-100 mb-5 rounded-pill">Sign In</Button>
					}
					<Nav.Link className="text-center" as={ Link } to="/users/signup">Don't have an account yet?</Nav.Link>
				</div>	
			</Form>
		</section>
		: <Navigate to='/' />
	);
};