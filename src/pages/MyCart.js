import { useState, useEffect, useContext } from 'react';
// import { Navigate } from 'react-router-dom';
// import { Modal, Form } from 'react-bootstrap';
import MyCartCard from '../components/MyCartCard';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function MyCart() {

	// getting global state/value of id and isAdmin
    const { idGlobal, isAdminGlobal } = useContext(UserContext);

    // init states/values 
	const [items, setItems] = useState([]);
	// const [isOpen, setIsOpen] = useState(false);
	// const [fullName, setFullName] = useState('');
	

	const getItemsFromCart = () => {

		fetch(`${process.env.REACT_APP_API_URL}products/view-my-cart`, {
			
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setItems(data.map(item => {

				return (
					<MyCartCard key={item.sku} prop={item} getItemsFn={getItemsFromCart}/>
				)
			}))
		})
		.catch(error => console.log(error))
		return;
	}	

	useEffect(() => {

		if (isAdminGlobal === false) {
			// fetch(`${process.env.REACT_APP_API_URL}products/view-my-cart`, {
			getItemsFromCart()
		}
	}, [isAdminGlobal]);

	// const showModal = () => {

	// 	setIsOpen(true);
	// };

	// const hideModal = () => {

	// 	setIsOpen(false);
	// };

	const handleSubmitOrder = (e) => {

		e.preventDefault()

		// fetch(`${process.env.REACT_APP_API_URL}products/checkout`, {
		fetch(`${process.env.REACT_APP_API_URL}products/checkout`, {

			method: 'POST',

			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			if(data.result === true) {

				getItemsFromCart()
				Swal.fire({
					title: "Order Successfully Submitted!",
					icon: "success"
				})
				return
			}
		})
	}

	return (
		<div className="container-fluid">
            <div className="row">
            	{/*<aside className="col-lg-3 p-5 bg-light">
            		{
            			(items.length === 0)
            			? <button type="button" className="btn btn-outline-danger rounded-pill w-100 mb-2" disabled>Submit Order</button>
            			: <button type="button" className="btn btn-outline-danger rounded-pill w-100 mb-2" onClick={e => handleSubmitOrder(e)}>Submit Order</button>
            		}
            		<Modal show={isOpen} onHide={hideModal}>
            			<Form>
	            			<Modal.Header>
	            				<Modal.Title>
	            					<h4>Order Summary</h4>
	            				</Modal.Title>
	            			</Modal.Header>
	            			<Modal.Body>
	            				
	            			</Modal.Body>
            			</Form>
            		</Modal>
            	</aside>*/}
                <main className="col p-5 bg-light overflow-auto" style={{maxHeight: "89vh" }}>
                	<table className="table table-striped table-bordered">
                		<thead>
                			<tr className="text-center">
                				<th>Brand</th>
                				<th>Model</th>
                				<th>Style</th>
                				<th>Color</th>
                				<th>SKU</th>
                				<th>Price</th>
                				<th>Qty</th>
                				<th>Subtotal</th>
                				<th>
                					{
                						(items.length === 0)
                						? <button type="button" className="btn btn-danger rounded-pill w-100" disabled>Submit Order</button>
                						: <button type="button" className="btn btn-danger rounded-pill w-100" onClick={e => handleSubmitOrder(e)}>Submit Order</button>
                					}
                				</th>
                			</tr>
                		</thead>
                		<tbody>
                			{items}
                		</tbody>
                	</table>
                    {/*<div className="row justify-content-center">
                        {items}
                    </div>*/}

                </main>
            </div>
        </div> 
	);
};