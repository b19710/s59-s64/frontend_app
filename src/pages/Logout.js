// libraries
import { useEffect, useContext } from 'react'; 
import { Navigate } from 'react-router-dom';

// context
import UserContext from '../UserContext';

export default function Logout() {

	// getting global functions
	const { unsetToken, setIdGlobal, setIsAdminGlobal } = useContext(UserContext);
	
	// to unset token
	unsetToken();

	// side effects to be applied to idGlobal and isAdminGlobal
	useEffect(() => {

		setIdGlobal(prevValue => null);
		setIsAdminGlobal(prevValue => null);

	}, [setIdGlobal, setIsAdminGlobal]);

	return (
		// after logging out, will then be redirected to the home page
		<Navigate to="/" />
	);
};