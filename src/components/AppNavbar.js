// libraries
import { useContext } from 'react';
import { Link } from 'react-router-dom';

// styles
import { Container, Navbar, Nav } from 'react-bootstrap';
import '../styles/AppNavbar.css';

// context
import UserContext from '../UserContext';

export default function AppNavbar() {
	
	// getting global state/value of id and isAdmin
	const { idGlobal, isAdminGlobal } = useContext(UserContext);

	return (
		<Navbar collapseOnSelect expand="lg" className="fixed-top navbar-dark bg-dark">
			<Container>
				<Navbar.Brand as={ Link } to="/"><h2 className="mb-0">MOTO<span className="red-this">FIT</span></h2></Navbar.Brand>
				<Navbar.Toggle aria-controls="responsive-navbar-nav" />
				<Navbar.Collapse id="responsive-navbar-nav">
					<Nav className="me-auto w-100 justify-content-end">
						<Nav.Link as={ Link } to="/" className="rounded-pill">Home</Nav.Link>
						<Nav.Link as={ Link } to="/products" className="rounded-pill">Products</Nav.Link>

					{/*condtional rendering*/}
					{
						(idGlobal === null)
						? <>
							<Nav.Link as={ Link } to="/users/login" className="rounded-pill">Login</Nav.Link>
							<Nav.Link as={ Link } to="/users/signup" className="rounded-pill">Sign Up</Nav.Link>
						</>
						: (isAdminGlobal)
							? <>
								<Nav.Link as={ Link } to="/users/admin/view-users" className="rounded-pill">Users</Nav.Link>
								<Nav.Link as={ Link } to="/users/admin/view-orders" className="rounded-pill">Orders</Nav.Link>
								<Nav.Link as={ Link } to="/users/logout" className="rounded-pill">Logout</Nav.Link>
							</>
							: <>
								
								<Nav.Link as={ Link } to="/products/view-my-cart" className="rounded-pill">My Cart</Nav.Link>
								<Nav.Link as={ Link } to="/users/order" className="rounded-pill">My Order</Nav.Link>
								<Nav.Link as={ Link } to="/users/logout" className="rounded-pill">Logout</Nav.Link>
							</>
					}
					{/*end of conditional rendering*/}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
};