import { useState, useContext } from 'react';
import {Modal, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductCard({prop, getAllProdsFn}) {
  
  /*
   * Getting global state
   */
  const { isAdminGlobal } = useContext(UserContext);

  /*
   * Init state/value for modal
   */
  const [isOpen, setIsOpen] = useState(false);

  /*
   * Init state/value for Edit Product Form
   */
  const [edtBrand, setEdtBrand] = useState(() => prop.brand);
  const [edtStyle, setEdtStyle] = useState(() => prop.style);
  const [edtModel, setEdtModel] = useState(() => prop.model);
  const [edtColor, setEdtColor] = useState(() => prop.color);
  const [edtSku, setEdtSku] = useState(()=> prop.sku);
  const [edtPrice, setEdtPrice] = useState(() => prop.price);
  const [edtStocks, setEdtStocks] = useState(() => prop.stocks);
  const [edtIsActive, setEdtIsActive] = useState(() => prop.isActive);

  /*const [isOpen2, setIsOpen2] = useState(false);
  const [qty, setQty] = useState(1);*/

  const showModal = () => {

    setIsOpen(prevState => true);
    return;
  };

  const hideModal = () => {

    setIsOpen(prevState => false);
    return 
  };

  const handleEditProduct = (e) => {

    e.preventDefault();

    // return fetch(`${process.env.REACT_APP_API_URL}products/edit`, {
    fetch(`${process.env.REACT_APP_API_URL}products/edit`, {

      method: 'PUT',
      
      headers: {
        'Content-Type':'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },

      body: JSON.stringify({
        id: prop._id,
        brand: edtBrand,
        style: edtStyle,
        model: edtModel,
        color: edtColor,
        sku: edtSku,
        price: edtPrice,
        stocks: edtStocks,
        isActive: edtIsActive
      })
    },)
    .then(res => res.json())
    .then(data => {

      if (data.result === true) {

        hideModal();
        getAllProdsFn();

        Swal.fire({
          title: 'Successfully Save Changes!',
          icon: 'success',
        });
        return
      };

    });
  };

/*  const showModal2 = () => {

    return setIsOpen2(true);
  };

  const hideModal2 = () => {

    return setIsOpen2(false);
  };

  

  const handleAddToCart = (e) => {

    e.preventDefault();

    // return fetch(`${process.env.REACT_APP_API_URL}products/add-to-cart`, {
    return fetch('http://localhost:4000/products/to-cart', {
      
      method: 'POST',

      headers: {
        'Content-Type':'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },

      body: JSON.stringify({
        sku: sku,
        qty: qty,
      })
    })
    .then(res => res.json())
    .then(data => {
      if (data.result === true) {

        hideModal2();

        return Swal.fire({
          title: 'Added To Cart Successfully!',
          icon: 'success',
        });
      };
    })*/

	return (
    (isAdminGlobal) ?
    <>
    <tr className="text-center">
      <td className="fw-bold">{prop._id}</td>
      <td>{prop.brand}</td>
      <td>{prop.style}</td>
      <td>{prop.model}</td>
      <td>{prop.color}</td>
      <td>{prop.sku}</td>
      <td>{prop.price}</td>
      <td>{prop.stocks}</td>
      <td>{(prop.isActive) ? "Available" : "Unavailable"}</td>
      <td><button type="button" className="btn btn-outline-secondary rounded-pill btn-sm w-100" onClick={showModal}>Edit</button></td>
    </tr>

    {/* MODAL FOR EDITTING PRODUCT */}
    <Modal show={isOpen} onHide={hideModal}>
      <Form onSubmit={e => handleEditProduct(e)}>
      <Modal.Header>
        <Modal.Title><h4>Edit Product</h4></Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form.Group>
            <Form.Label className="m-0">* Brand:</Form.Label>
            <Form.Control 
                className="mb-2"
                type="text"
                value={edtBrand}
                onChange={e => setEdtBrand(prevState => e.target.value)}
                placeholder="* Brand (e.g. SMK, AVG, LS2)"/>
            <Form.Label className="m-0">* Style:</Form.Label>
            <Form.Select aria-label="Default select example" className="mb-2" value={edtStyle} onChange={e => setEdtStyle(prevState => e.target.value)}>
                <option>Select Style</option>
                <option value="Full-face">Full Face</option>
                <option value="Half-face">Half Face</option>
                <option value="Modular">Modular</option>
                <option value="Off road">Off Road</option>
            </Form.Select>
            <Form.Label className="m-0">* Model:</Form.Label>
            <Form.Control 
                className="mb-2"
                type="text"
                value={edtModel}
                onChange={e => setEdtModel(prevState => e.target.value)}
                placeholder="* Model Name" />
            <Form.Label className="m-0">* Color:</Form.Label>
            <Form.Control 
                className="mb-2"
                type="text"
                value={edtColor}
                onChange={e => setEdtColor(prevState => e.target.value)}
                placeholder="* Color" />
            <Form.Label className="m-0">* SKU:</Form.Label>
            <Form.Control 
                className="mb-2"
                type="text"
                value={edtSku}
                onChange={e => setEdtSku(prevState => e.target.value)}
                placeholder="* SKU (e.g. EXHE-STEXS000)" />
            <Form.Label className="m-0">* Price:</Form.Label>
            <Form.Control 
                className="mb-2"
                type="text"
                value={edtPrice}
                onChange={e => setEdtPrice(prevState => e.target.value)}
                placeholder="* Price" />
            <Form.Label className="m-0">* Stocks:</Form.Label>
            <Form.Control 
                className="mb-2"
                type="number"
                value={edtStocks}
                onChange={e => setEdtStocks(e.target.value)}
                placeholder="* Stocks" />
            <Form.Label className="m-0">* Status:</Form.Label>
            <Form.Select aria-label="Default select example" className="mb-2" value={edtIsActive} onChange={e => setEdtIsActive(prevState => e.target.value)}>
                <option>Select Status</option>
                <option value="true">Available</option>
                <option value="false">Unavailable</option>
            </Form.Select>
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
          <Button variant="outline-danger rounded-pill" type="submit">Save</Button>
          <Button variant="outline-danger rounded-pill" onClick={hideModal}>Cancel</Button>
      </Modal.Footer>
      </Form>
    </Modal>
    </>
    :
    <>
      
    </>
    /*(user.id === null)
    ?*/ /*<>
      <div className="col-lg-4">
        <Card className="container">
          <Card.Body>
            <Card.Title>{brand}</Card.Title>
            <Card.Subtitle className="mb-3">{`${model} | ${color}`}</Card.Subtitle>
            <Card.Text> Price: PHP {price}.00</Card.Text>
            <button type="button" className="btn btn-outline-danger rounded-pill w-100" onClick={showModal}>View Details</button>
            <Modal show={isOpen} onHide={hideModal}>
              <Modal.Header>
                <Modal.Title><h4>{brand} ({`${model} | ${color}`})</h4></Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Table striped >
                  <tbody >
                    <tr>
                      <td>Details:</td>
                      <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla in congue erat. In hac habitasse platea dictumst. Cras at enim mauris. Nulla rhoncus tempor ultrices.</td>
                    </tr>
                    <tr>
                      <td>Price:</td>
                      <td>PHP {price}.00</td>
                    </tr>
                    <tr>
                      <td>Style:</td>
                      <td>{style}</td>
                    </tr>
                    <tr>
                      <td>SKU:</td>
                      <td>{sku}</td>
                    </tr>
                  </tbody>
                </Table>
              </Modal.Body>
              <Modal.Footer>
                  <Button variant="outline-danger rounded-pill" onClick={hideModal}>Close</Button>
              </Modal.Footer>
            </Modal>
          </Card.Body>
        </Card>
      </div>
    </>*/
    /*: (user.isAdmin)
      ? <>
        <section className="container overflow-auto">
          <div className="row justify-content-center">
            <div className="col-lg-10">
              <Card className="container">
                <Card.Body>
                  <Card.Title>{`Brand: ${brand}`}</Card.Title>
                  <Card.Subtitle className="mb-3">{`Product ID: ${_id}`}</Card.Subtitle>
                    <Table striped >
                      <tbody >
                        <tr>
                          <td>Style:</td>
                          <td>{style}</td>
                        </tr>
                        <tr>
                          <td>Model:</td>
                          <td>{model}</td>
                        </tr>
                        <tr>
                          <td>Size And Color:</td>
                          <td>{`${size} | ${color}`}</td>
                        </tr>
                        <tr>
                          <td>SKU:</td>
                          <td>{sku}</td>
                        </tr>
                        <tr>
                          <td>Quantity and Price:</td>
                          <td>{`${stocks} | ${price}`}</td>
                        </tr>
                        <tr>
                          <td>Active:</td>
                          <td>{(isActive) ? 'Yes' : 'No'}</td>
                        </tr>
                      </tbody>
                    </Table>
                  <button type="button" className="btn btn-outline-danger rounded-pill" onClick={showModal}>Edit</button>
                  
                </Card.Body>
              </Card>
            </div>
          </div>
        </section>
      </>
      : <>
        <div className="col-lg-4">
          <Card className="container">
            <Card.Body>
              <Card.Title>{brand}</Card.Title>
              <Card.Subtitle className="mb-3">{`${model} | ${color}`}</Card.Subtitle>
              <Card.Text> Price: PHP {price}.00</Card.Text>
              <button type="button" className="btn btn-outline-danger rounded-pill w-100 mb-1" onClick={showModal2}>Add To Cart</button>
              <button type="button" className="btn btn-outline-danger rounded-pill w-100" onClick={showModal}>View Details</button>
              <Modal show={isOpen2} onHide={hideModal2}>
                <Form onSubmit={e => handleAddToCart(e)}>
                  <Modal.Header>
                    <Modal.Title><h4>{brand} ({`${model} | ${color} - PHP ${price}.00`}) </h4></Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <Form.Group>
                      <Form.Control
                        className="mb-2" 
                        type="text"
                        value={sku} 
                        readOnly/>
                      <Form.Control
                        type="number"
                        value={qty}
                        onChange={e => setQty(e.target.value)} />
                    </Form.Group>  
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="outline-danger rounded-pill" type="submit">Add</Button>
                  </Modal.Footer>
                </Form>
              </Modal>
              <Modal show={isOpen} onHide={hideModal}>
                <Modal.Header>
                  <Modal.Title><h4>{brand} ({`${model} | ${color}`})</h4></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <Table striped >
                    <tbody >
                      <tr>
                        <td>Details:</td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla in congue erat. In hac habitasse platea dictumst. Cras at enim mauris. Nulla rhoncus tempor ultrices.</td>
                      </tr>
                      <tr>
                        <td>Price:</td>
                        <td>PHP {price}.00</td>
                      </tr>
                      <tr>
                        <td>Style:</td>
                        <td>{style}</td>
                      </tr>
                      <tr>
                        <td>SKU:</td>
                        <td>{sku}</td>
                      </tr>
                    </tbody>
                  </Table>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-danger rounded-pill" onClick={hideModal}>Close</Button>
                </Modal.Footer>
              </Modal>
            </Card.Body>
          </Card>
        </div>
      </>*/
	);
};