// libraries
import { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';

// pages and components
import Home from './pages/Home';
import Login from './pages/Login';
import Signup from './pages/Signup';
import Products from './pages/Products';
import MyCart from './pages/MyCart';
import Logout from './pages/Logout';
import AppNavbar from './components/AppNavbar';

// styles
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

// context
import { UserProvider } from './UserContext';

function App() {

  // setting global state/value that will be used across pages
  const [idGlobal, setIdGlobal] = useState(() => null);
  const [isAdminGlobal, setIsAdminGlobal] = useState(() => null);

  // fn to call for when logging out (global fn)
  const unsetToken = () => {

    localStorage.clear();
    return;
  };

  useEffect(() => {

    // conditional statement for when to fetch details of the user
    if (localStorage.getItem('token') === null) {

      setIdGlobal(prevValue => null);
      setIsAdminGlobal(prevVale => null);
    } else {

      fetch(`${process.env.REACT_APP_API_URL}users/details`, {

        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        }
      })
      .then(res => res.json())
      .then(data => {
        
        setIdGlobal(prevValue => data._id);
        setIsAdminGlobal(prevVale => data.isAdmin);
        return;
      })
      .catch(error => console.log(error))
    }
  }, [setIdGlobal, setIsAdminGlobal]);

  return (
    <UserProvider value={{idGlobal, isAdminGlobal, setIdGlobal, setIsAdminGlobal, unsetToken}}>
    <Router>
      <AppNavbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/users/login" element={<Login />} />
        <Route path="/users/signup" element={<Signup />} />
        <Route path="/products/view-my-cart" element={<MyCart />} />
        <Route path="/products" element={<Products />} />
        <Route path="/users/logout" element={<Logout />} />
      </Routes>
    </Router>
    </UserProvider>
  );
};

export default App;
